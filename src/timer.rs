use rand::Rng;

use crate::{
    context::{COLUMN, ROW},
    enemy::Enemy,
    entities::{self, cell::Cell},
    events::Event,
};

use super::context::FIELD;
use super::controller::{_move, put_bomb};

type EventLog = (usize, usize, Event);

pub fn handle(cells: &mut FIELD, enemies: &mut Vec<Enemy>) {
    handle_field(cells);

    handle_enemies(cells, enemies);
}

// フィールドに関する処理
fn handle_field(cells: &mut FIELD) {
    let mut event_log_collecter: Vec<EventLog> = vec![];

    for (row_index, row) in cells.iter().enumerate() {
        for (column_index, cell) in row.iter().enumerate() {
            // 爆弾チェック

            if let Some(c) = cell {
                let events = c.arrangement.handle_time();

                let mut event_logs = events
                    .into_iter()
                    .map(|e| (column_index, row_index, e))
                    .collect::<Vec<EventLog>>();
                event_log_collecter.append(&mut event_logs);
            }
        }
    }

    while let Some((x, y, event)) = event_log_collecter.pop() {
        match event {
            crate::events::Event::EXPLOSION => explode(x, y, cells),
            Event::PUTTING_OUT => putting_out(x, y, cells),
        }
    }
}

pub fn handle_enemies(cells: &mut FIELD, enemies: &mut Vec<Enemy>) {
    let mut thread_rng = rand::thread_rng();

    for enemy in enemies.iter_mut() {
        let diff_x = random_between_minus1_1();
        let diff_y = random_between_minus1_1();

        let new_position = _move(cells, enemy.position, diff_x, diff_y);
        enemy.position = new_position;

        // 1/20で爆弾配置
        if thread_rng.gen_ratio(1, 100) {
            put_bomb(cells, enemy.position.0, enemy.position.1, 3);
        }
    }
}

fn random_between_minus1_1() -> isize {
    if rand::random() || rand::random() {
        return 0;
    }

    if rand::random() {
        1
    } else {
        -1
    }
}

// 爆発
fn explode(point_x: usize, point_y: usize, cells: &mut FIELD) {
    let generate_explosion = || Some(Cell::new(Box::new(entities::explosion::Explosion::new())));

    cells[point_y][point_x] = generate_explosion();

    'a: for diff_x in 1..COLUMN {

        let can_fire_spread = cells[point_y][point_x + diff_x].is_some();
        let can_induction = match cells[point_y][point_x + diff_x] {
            Some(ref cell) => cell.arrangement.can_induction(),
            None => false,
        };

        let can_burn = match cells[point_y][point_x + diff_x] {
            Some(ref cell) => cell.arrangement.can_burn(),
            None => true,
        };

        if can_induction {
            explode(point_x + diff_x, point_y, cells);
        }
        if can_burn {
            cells[point_y][point_x + diff_x] = generate_explosion();
        }
        // 何か存在した場合はそこで延焼が終了
        if can_fire_spread {
            break 'a;
        }
    }

    'a: for diff_x in 1..COLUMN {
        let can_fire_spread = cells[point_y][point_x - diff_x].is_some();

        let can_induction = match cells[point_y][point_x - diff_x] {
            Some(ref cell) => cell.arrangement.can_induction(),
            None => false,
        };
        let can_burn = match cells[point_y][point_x - diff_x] {
            Some(ref cell) => cell.arrangement.can_burn(),
            None => true,
        };

        if can_induction {
            explode(point_x - diff_x, point_y, cells);
        }

        if can_burn {
            cells[point_y][point_x - diff_x] = generate_explosion();
        }
        // 何か存在した場合はそこで延焼が終了
        if can_fire_spread {
            break 'a;
        }
    }

    'a: for diff_y in 1..ROW {
        let can_fire_spread = cells[point_y + diff_y][point_x].is_some();

        let can_induction = match cells[point_y + diff_y][point_x] {
            Some(ref cell) => cell.arrangement.can_induction(),
            None => false,
        };

        let can_burn = match cells[point_y + diff_y][point_x] {
            Some(ref cell) => cell.arrangement.can_burn(),
            None => true,
        };
        if can_induction {
            explode(point_x, point_y + diff_y, cells);
        }
        if can_burn {
            cells[point_y + diff_y][point_x] = generate_explosion();

        }
        // 何か存在した場合はそこで延焼が終了
        if can_fire_spread {
            break 'a;
        }
    }

    'a: for diff_y in 1..ROW {
        let can_fire_spread = cells[point_y - diff_y][point_x].is_some();

        let can_induction = match cells[point_y - diff_y][point_x] {
            Some(ref cell) => cell.arrangement.can_induction(),
            None => false,
        };

        let can_burn = match cells[point_y - diff_y][point_x] {
            Some(ref cell) => cell.arrangement.can_burn(),
            None => true,
        };

        if can_induction {
            explode(point_x, point_y - diff_y, cells);
        }
        if can_burn {
            cells[point_y - diff_y][point_x] = generate_explosion();
        }
        // 何か存在した場合はそこで延焼が終了
        if can_fire_spread {
            break 'a;
        }
    }
}

fn putting_out(point_x: usize, point_y: usize, cells: &mut FIELD) {
    cells[point_y][point_x] = None;
}

mod test {

    use crate::{
        context,
        entities::{arrangement::Arrangement, hard_block::HardBlock, soft_block::SoftBlock},
        make_initial_field,
    };

    use super::*;

    #[test]
    fn test_soft_block() {
        let block = SoftBlock::new();

        assert!(!block.can_burn());
        assert!(block.can_induction());

        let mut field = make_initial_field();
        explode(1, 3, &mut field);

        dbg!(&field[2][1]);
        dbg!(&field[3][1]);
        dbg!(&field[4][1]);
    }
}
