
use context::{COLUMN, ROW};
use entities::hard_block::HardBlock;

extern crate termion;

pub mod context;
pub mod controller;
pub mod entities;
pub mod events;
pub mod timer;
pub mod result_checker;
pub mod enemy;

use controller::PlayerCommand;
use entities::cell::Cell;
use entities::soft_block::SoftBlock;
use std::io::stdout;
use std::io::Write;
use std::process;
use std::sync::Arc;
use std::sync::Mutex;


pub fn init() {
    let fields = make_initial_field();
    let stdout = stdout();

    let mut context = context::Context::new(fields, stdout);

    let commands_wrapper: Arc<Mutex<Vec<Box<dyn PlayerCommand + Send>>>> =
        Arc::new(Mutex::new(vec![]));
    let commands_wrapper_cloned = Arc::clone(&commands_wrapper);

    std::thread::spawn(move || {
        controller::Controller::init(commands_wrapper_cloned);
    });

    loop {
        std::thread::sleep(std::time::Duration::from_millis(50));

        let mut commands = commands_wrapper.lock().unwrap();

        while let Some(command) = commands.pop() {
            // command.display();
            command.handle(&mut context);
        }

        timer::handle(&mut context.cells.borrow_mut(), &mut context.enemies);

        write!(context.stdout.stdout, "{}", termion::clear::All).unwrap();
        writeln!(
            context.stdout.stdout,
            "{}",
            context::Fields::new(&mut context.cells.borrow_mut())
        ).unwrap();
        writeln!(context.stdout.stdout, "{}", context.player).unwrap();
        // FIXME: Vec<Enemy>を束ねて出力したい
        for e in context.enemies.iter() {
            writeln!(context.stdout.stdout, "{}", e).unwrap();
        }
        writeln!(context.stdout.stdout, "{}", termion::cursor::Goto(1, 1)).unwrap();
        if let Some(result) = result_checker::check_result(&context.cells.borrow(), &mut context.player,&mut  context.enemies) {
            writeln!(context.stdout.stdout, "{}", result).unwrap();
            break;
        }
    }

    std::thread::sleep(std::time::Duration::from_millis(5000));
    process::exit(0);
}


// 初期のマップ生成
fn make_initial_field() -> context::FIELD {
    let mut fields: context::FIELD = Default::default();
    // let mut fields: context::FIELD<dyn Arrangement> = [[None; context::ROW]; context::COLUMN];

    for x in 0..COLUMN {
        fields[0][x] = Some(Cell::new(Box::new(HardBlock::new())));
        fields[ROW - 1][x] = Some(Cell::new(Box::new(HardBlock::new())));
    }
    for y in 0..ROW {
        fields[y][0] = Some(Cell::new(Box::new(HardBlock::new())));
        fields[y][COLUMN - 1] = Some(Cell::new(Box::new(HardBlock::new())));
    }
    for y in 0..(ROW / 2 - 1) {
        for x in 0..(COLUMN / 2 - 1) {
            fields[2 * y + 2][2 * x + 2] = Some(Cell::new(Box::new(HardBlock::new())));
        }
    }

    fields[1][4] = Some(Cell::new(Box::new(SoftBlock::new())));
    fields[2][3] = Some(Cell::new(Box::new(SoftBlock::new())));
    fields[3][2] = Some(Cell::new(Box::new(SoftBlock::new())));
    fields[4][1] = Some(Cell::new(Box::new(SoftBlock::new())));

    fields
}

mod test {
    
    use super::*;

    #[test]
    fn test_display_map() {
        let mut fields = make_initial_field();
        let fields_wrapper = context::Fields::new(&mut fields);
        // print!("{}", fields_wrapper);

        let c = format!("{}", fields_wrapper);

        println!("{}", c);
    }
}
