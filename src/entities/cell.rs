use std::fmt::{self, Debug};

use super::arrangement::Arrangement;

#[derive(Debug)]
pub struct Cell {
    pub arrangement: Box<dyn Arrangement>,
}

impl Cell {
    pub fn new(arrangement: Box<dyn Arrangement>) -> Cell {
        Cell {
            arrangement,
        }
    }
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.arrangement, f)
    }
}
