
use std::{
    fmt::{self},
    time::{self, SystemTime},
};
use termion::color;

use super::arrangement::Arrangement;

use crate::events::Event;

// 爆弾
#[derive(Clone, Copy, Debug)]
pub struct Explosion {
    started_at: SystemTime,
}
impl Explosion {
    pub fn new() -> Self {
        Self {
            started_at: SystemTime::now(),
        }
    }
}
impl Arrangement for Explosion {
    fn can_move(&self) -> bool {
        true
    }

    fn can_burn(&self) -> bool {
        true
    }

    fn char(&self) -> String {
        "Q".to_string()
    }

    fn fmt_(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}{}",
            color::Bg(color::Blue),
            " ",
            color::Bg(color::Reset)
        )
    }

    fn handle_time(&self) -> Vec<Event> {
        // 鎮火処理
        let now = time::SystemTime::now();
        let duration = std::time::Duration::from_millis(1000);
        use std::ops::Add;

        if self.started_at.add(duration) < now {
            return vec![Event::PUTTING_OUT];
        }
        vec![]
    }

    fn whether_to_die(&self) -> bool {
        true
    }
}
