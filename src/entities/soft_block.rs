
use super::arrangement::Arrangement;

// ソフトブロック
#[derive(Clone, Copy, Debug)]
pub struct SoftBlock {}
impl SoftBlock {
    pub fn new() -> Self {
        Self {}
    }
}
impl Arrangement for SoftBlock {
    fn can_move(&self) -> bool {
        false
    }

    fn can_induction(&self) -> bool {
        false
    }

    fn can_burn(&self) -> bool {
        true
    }

    fn char(&self) -> String {
        "□".to_string()
    }
}
