
use std::{
    fmt::{self},
    time::{self, SystemTime},
};
use termion::color;

use super::arrangement::Arrangement;

use crate::events::Event;

// 爆弾
#[derive(Clone, Copy, Debug)]
pub struct Bomb {
    put_at: SystemTime,
    fire: usize,
}
impl Bomb {
    pub fn new(fire: usize, put_at: SystemTime) -> Self {
        Self { fire, put_at }
    }
}
impl Arrangement for Bomb {
    fn can_move(&self) -> bool {
        false
    }

    fn can_burn(&self) -> bool {
        true
    }

    fn can_induction(&self) -> bool {
        true
    }

    fn char(&self) -> String {
        "Q".to_string()
    }

    fn fmt_(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}{}",
            color::Bg(color::Red),
            " ",
            color::Bg(color::Reset)
        )
    }

    // 爆発
    fn handle_time(&self) -> Vec<Event> {
        let now = time::SystemTime::now();
        let duration = std::time::Duration::from_millis(2000);
        use std::ops::Add;

        if self.put_at.add(duration) < now {
            return vec![Event::EXPLOSION];
        }

        vec![]
    }
}
