use std::{
    fmt::{self},
};

use crate::events::Event;

pub trait Arrangement {
    // fn new() -> Self;

    fn can_move(&self) -> bool;

    // 燃えるかどうか
    fn can_burn(&self) -> bool;

    // 誘爆するか
    fn can_induction(&self) -> bool {
        false
    }

    fn char(&self) -> String;

    fn can_put_bomb(&self) -> bool {
        false
    }

    fn fmt_(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.char())
    }

    fn handle_time(&self) -> Vec<Event> {
        vec![]
    }

    fn whether_to_die(&self) -> bool {
        false
    }
}

impl fmt::Display for dyn Arrangement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt_(f)
    }
}
impl fmt::Debug for dyn Arrangement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.char())
    }
}
