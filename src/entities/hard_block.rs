use super::arrangement::Arrangement;
use std::{
    fmt::{self}
};

// ハードブロック
#[derive(Clone, Copy, Debug)]
pub struct HardBlock {}
impl HardBlock {
    pub fn new() -> Self {
        Self {}
    }
}
impl Arrangement for HardBlock {
    fn can_move(&self) -> bool {
        false
    }

    fn can_burn(&self) -> bool {
        false
    }

    fn char(&self) -> String {
        "■".to_string()
    }

    fn fmt_(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.char())
    }
}

mod test {

    use super::*;

    #[test]
    fn test_block() {
        let block = HardBlock::new();

        assert!(!block.can_move());
        assert!(!block.can_burn());
    }
}
