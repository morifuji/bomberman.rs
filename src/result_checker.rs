
use crate::enemy::Enemy;
use std::fmt::{self};
use super::context::{FIELD, Player};

fn is_dying(cells: &FIELD, position: (usize, usize)) -> bool {
    match cells[position.1][position.0] {
        Some(ref c) => c.arrangement.whether_to_die(),
        None => false,
    }
}

pub enum GameResult {
    PLAYER_WIN,
    PLAYER_LOSE
}
impl fmt::Display for GameResult {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        let message = match *self {
            Self::PLAYER_WIN => format!("{}{}You win !!!!!",termion::color::Bg(termion::color::Black),termion::color::Fg(termion::color::White)),
            Self::PLAYER_LOSE => format!("{}{}GAME OVER....",termion::color::Bg(termion::color::Black),termion::color::Fg(termion::color::Red)),
        };

        write!(
            f,
            "{}{}{}",
            termion::clear::All,
            termion::cursor::Goto(5, 5),
            message
        )
    }
}


pub fn check_result(cells: &FIELD, player: &mut Player, enemies: &mut Vec<Enemy>) -> Option<GameResult> {

    let is_collision = enemies.iter().any(|e| player.position == e.position);

    // プレイヤー敗北
    if is_collision || is_dying(cells, player.position) {
        return Some(GameResult::PLAYER_LOSE);
    }

    enemies.retain(|e| !is_dying(cells, e.position));

    // 敵全滅した？    
    if enemies.is_empty() {return Some(GameResult::PLAYER_WIN)}

    None
}
