use super::context::FIELD_START_OFFSET;
use std::fmt;

#[derive(Debug, Clone, Copy)]
pub struct Enemy {
    pub position: (usize, usize),
}
impl fmt::Display for Enemy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}",
            termion::cursor::Goto(
                self.position.0 as u16 + 1,
                (FIELD_START_OFFSET + self.position.1) as u16
            ),
            "X"
        )
    }
}