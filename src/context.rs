use super::entities::arrangement::Arrangement;
use crate::entities::cell::Cell;
use std::cell::RefCell;
use std::fmt::{self, Debug};
use std::io::Stdout;
use termion::raw::IntoRawMode;
use super::enemy::Enemy;

pub const ROW: usize = 15;
pub const COLUMN: usize = 15;
pub type FIELD = [[Option<Cell>; ROW]; COLUMN];
pub const FIELD_START_OFFSET: usize = 5;

pub struct MyStdout {
    pub stdout: termion::raw::RawTerminal<std::io::Stdout>,
}

impl fmt::Debug for MyStdout {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "hello.")
    }
}

#[derive(Debug)]
pub struct Context {
    pub arrangements: Vec<Box<dyn Arrangement>>,

    pub cells: RefCell<FIELD>,

    pub stdout: MyStdout,

    pub player: Player,

    pub enemies: Vec<Enemy>,
}

#[derive(Debug, Clone, Copy)]
pub struct Player {
    pub position: (usize, usize),
}
impl fmt::Display for Player {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}",
            termion::cursor::Goto(
                self.position.0 as u16 + 1,
                (FIELD_START_OFFSET + self.position.1) as u16
            ),
            "@"
        )
    }
}


pub struct Fields<'a> {
    fields: &'a mut FIELD,
}

impl<'a> Fields<'a> {
    pub fn new(fields: &'a mut FIELD) -> Self {
        Self { fields }
    }
}
impl<'a> fmt::Display for Fields<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "\n{}",
            termion::cursor::Goto(1, FIELD_START_OFFSET as u16)
        ).unwrap();

        for row in self.fields.iter() {
            for cell_result in row.iter() {
                match cell_result {
                    Some(ref cell) => std::fmt::Display::fmt(cell, f),
                    None => write!(f, " "),
                }
                .expect("error happen when write cell");
            }

            write!(f, "\n{}", termion::cursor::Left(999))
                .expect("error happen when write newline.");
        }

        write!(f, "")
    }
}

impl Context {
    pub fn new(cells: FIELD, stdout: Stdout) -> Self {

        let enemies: Vec<Enemy> = (0..5).map(|i| {
            Enemy{
                position: (2*i+3, 2*i+3)
            }
        }).collect();

        Context {
            arrangements: vec![],
            cells: RefCell::new(cells),
            stdout: MyStdout {
                stdout: stdout.into_raw_mode().expect("can't get stdout"),
            },
            player: Player { position: (1, 1) },
            enemies
        }
    }
}
