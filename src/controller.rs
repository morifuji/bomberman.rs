use crate::context::Player;
use crate::entities::bomb::Bomb;
use crate::entities::cell::Cell;
use std::borrow::BorrowMut;
use std::io::{stdin};

use std::sync::{Arc, Mutex};
use termion::event::Key;
use termion::input::TermRead;
use super::context::FIELD;

use super::context::Context;
use std::fmt::{self, Debug};

pub trait PlayerCommand {
    fn handle(&self, context: &mut Context);

    fn display(&self);
}

impl fmt::Debug for dyn PlayerCommand + Sync {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt(f)
    }
}

fn plus_safe(a: usize, b: isize) -> usize {
    (b + (a as isize)) as usize
}

pub fn _move(cells: &mut FIELD, position: (usize, usize), x_diff: isize, y_diff: isize) -> (usize, usize){
    let x = plus_safe(position.0, x_diff);
    let y = plus_safe(position.1, y_diff);

    let cell_option = &cells[y][x];

    let can_move = match cell_option {
        Some(ref cell) => cell.arrangement.can_move(),
        None => true,
    };

    if !can_move {
        return position;
    }

    (x, y)
}

fn _move_player(context: &mut Context, x_diff: isize, y_diff: isize) {
    let new_position = _move(&mut context.cells.borrow_mut(), context.player.position, x_diff, y_diff);
    context.player = Player { position: new_position };
}

#[derive(Debug)]
struct MoveUp {}
impl PlayerCommand for MoveUp {
    fn handle(&self, context: &mut Context) {
        _move_player(context, 0, -1);
    }
    fn display(&self) {
        println!("MoveUp");
    }
}
impl fmt::Display for MoveUp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MoveUp")
    }
}

#[derive(Debug)]
struct MoveDown {}
impl PlayerCommand for MoveDown {
    fn handle(&self, context: &mut Context) {
        _move_player(context, 0, 1);
    }
    fn display(&self) {
        println!("MoveDown");
    }
}
impl fmt::Display for MoveDown {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MoveDown")
    }
}

#[derive(Debug)]
struct MoveLeft {}
impl PlayerCommand for MoveLeft {
    fn handle(&self, context: &mut Context) {
        _move_player(context, -1, 0);
    }
    fn display(&self) {
        println!("MoveLeft");
    }
}
impl fmt::Display for MoveLeft {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MoveLeft")
    }
}

#[derive(Debug)]
struct MoveRight {}
impl PlayerCommand for MoveRight {
    fn handle(&self, context: &mut Context) {
        _move_player(context, 1, 0);
    }
    fn display(&self) {
        println!("MoveRight");
    }
}
impl fmt::Display for MoveRight {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "MoveRight")
    }
}

pub fn put_bomb(cells: &mut FIELD, x: usize, y: usize, fire_power: usize) {

        let can_put_bomb = match cells[y][x] {
            Some(ref cell) => cell.arrangement.can_put_bomb(),
            None => true,
        };

        if !can_put_bomb {
            return;
        }

        let now = std::time::SystemTime::now();
        cells[y][x] = Some(Cell::new(Box::new(Bomb::new(fire_power, now))));
}

#[derive(Debug)]
struct PutBomb {}
impl PlayerCommand for PutBomb {
    fn handle(&self, context: &mut Context) {
        let (x, y) = context.player.position;
        let mut cells = context.cells.borrow_mut();

        put_bomb(cells.borrow_mut(), x, y, 3);
    }
    fn display(&self) {
        println!("PutBomb");
    }
}
impl fmt::Display for PutBomb {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PutBomb")
    }
}

pub struct Controller {}

impl Controller {
    pub fn init(commands: Arc<Mutex<Vec<Box<dyn PlayerCommand + Send>>>>) {
        let stdin = stdin();

        for c in stdin.keys() {
            // Clear the current line.

            let mut commands = commands.lock().unwrap();

            // Print the key we type...
            match c.unwrap() {
                // Exit.
                Key::Char('q') => std::process::exit(0),
                Key::Char('z') => commands.push(Box::new(PutBomb {})),
                Key::Alt(c) => println!("Alt-{}", c),
                Key::Ctrl(c) => println!("Ctrl-{}", c),
                Key::Left => commands.push(Box::new(MoveLeft {})),
                Key::Right => commands.push(Box::new(MoveRight {})),
                Key::Up => commands.push(Box::new(MoveUp {})),
                Key::Down => commands.push(Box::new(MoveDown {})),
                _ => println!("Other"),
            }

            // println!("{:?}", commands);
        }
    }
}

mod test {
    #[test]
    fn test_move() {}
}
